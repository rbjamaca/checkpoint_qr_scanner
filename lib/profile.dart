import 'dart:async';
import 'dart:convert';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Officer {
  String id;
  String name;
  String username;
  String email;

  Officer({this.id, this.name, this.username, this.email});

  Officer.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    username = json['username'];
    email = json['email'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['username'] = this.username;
    data['email'] = this.email;
    return data;
  }
}

class ProfilePage extends StatefulWidget {
  final String text;

  ProfilePage({Key key, @required this.text}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  String upName, upUsername, upEmail, upPass;
  Future<Officer> futureOfficer;
  bool isEnabled = false;
  bool isLoading = false;
  final _key = new GlobalKey<FormState>();
  bool _secureText = true;

  @override
  void initState() {
    super.initState();
    futureOfficer = fetchOfficer();
  }

  showHide() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  check() {
    final form = _key.currentState;
    if (form.validate()) {
      form.save();
      update();
    }
  }
  update() async {
    var text = widget.text;
    setState(() {
      isLoading = true;
    });
    final response = await http
        .post("http://192.168.137.1/travelpass/index.php/api/update", body: {
      "flag": 1.toString(),
      "name": upName,
      "email": upEmail,
      "username": upUsername,
      "password": upPass,
      "id": text,
      // "password": password,
      "fcm_token": "test_fcm_token"
    });
    

    final data = jsonDecode(response.body.toString());
    print(data);
    int value = data['value'];
    String message = data['message'];

    if (value == 1) {
      // setState(() {
      //   _loginStatus = LoginStatus.signIn;
      //   savePref(value, emailAPI, nameAPI, id);
      // });
      print(message);
      loginToast('success');
      enable();
    } else {
      print("fail");
      print(message);
      loginToast('success');
      enable();
    }
  }

  loginToast(String toast) {
    setState(() {
      isLoading = false;
    });
    return Fluttertoast.showToast(
        msg: toast,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.blueAccent,
        textColor: Colors.white);
  }

  enable() {
    if (isEnabled == false) {
      setState(() {
        isEnabled = true;
      });
    } else {
      setState(() {
        isEnabled = false;
      });
    }
  }

  Future<Officer> fetchOfficer() async {
    var text = widget.text;
    final response = await http
        .get('http://192.168.137.1/travelpass/index.php/API/profile/$text');
    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return Officer.fromJson(json.decode(response.body));
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Profile",
        ),
        backgroundColor: Color(0xFF214d8e),
        centerTitle: true,
      ),
      body: Center(
        child: FutureBuilder<Officer>(
          future: futureOfficer,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data.username != null) {
                return ListView(
                  shrinkWrap: true,
                  padding: const EdgeInsets.all(8.0),
                  children: <Widget>[
            Center(
              child: Container(
                padding: const EdgeInsets.all(8.0),
                color: Color(0xFF214d8e),
                child: Form(
                  key: _key,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                    new Image.asset(
                      'assets/cdeo logo.png',
                      scale: 4,
                      height: 150,
                    ),
                    Card(
                      elevation: 6.0,
                      child: TextFormField(
                        enabled: isEnabled ? true : false,
                        initialValue: snapshot.data.name,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Please Insert Name";
                          }
                          return null;
                        },
                        onSaved: (e) => upName = e,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w300,
                        ),
                        decoration: InputDecoration(
                            prefixIcon: Padding(
                              padding: EdgeInsets.only(left: 20, right: 15),
                              child: Icon(Icons.person, color: Colors.black),
                            ),
                            contentPadding: EdgeInsets.all(18),
                            labelText: "Name"),
                      ),
                    ),
                    Card(
                      elevation: 6.0,
                      child: TextFormField(
                        enabled: isEnabled ? true : false,
                        initialValue: snapshot.data.username,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Please Insert Username";
                          }
                          return null;
                        },
                        onSaved: (e) => upUsername = e,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w300,
                        ),
                        decoration: InputDecoration(
                            prefixIcon: Padding(
                              padding: EdgeInsets.only(left: 20, right: 15),
                              child:
                                  Icon(Icons.account_box, color: Colors.black),
                            ),
                            contentPadding: EdgeInsets.all(18),
                            labelText: "Username"),
                      ),
                    ),
                    Card(
                      elevation: 6.0,
                      child: TextFormField(
                        enabled: isEnabled ? true : false,
                        initialValue: snapshot.data.email,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Please Insert Email";
                          }
                          return null;
                        },
                        onSaved: (e) => upEmail = e,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w300,
                        ),
                        decoration: InputDecoration(
                            prefixIcon: Padding(
                              padding: EdgeInsets.only(left: 20, right: 15),
                              child: Icon(Icons.email, color: Colors.black),
                            ),
                            contentPadding: EdgeInsets.all(18),
                            labelText: "Email"),
                      ),
                    ),
                    Card(
                      elevation: 6.0,
                      child: TextFormField(
                        enabled: isEnabled ? true : false,
                        // initialValue: snapshot.data.email,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Please Insert Password";
                          }
                          return null;
                        },
                        onSaved: (e) => upPass = e,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w300,
                        ),
                        decoration: InputDecoration(
                            prefixIcon: Padding(
                              padding: EdgeInsets.only(left: 20, right: 15),
                              child: Icon(Icons.email, color: Colors.black),
                            ),
                            contentPadding: EdgeInsets.all(18),
                            labelText: "Password"),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(14.0),
                    ),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        SizedBox(
                          height: 44.0,
                          child: isEnabled
                              ? 
                                RaisedButton(
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(15.0)),
                                  child: Text(
                                    "Update",
                                    style: TextStyle(fontSize: 18.0),
                                  ),
                                  textColor: Colors.white,
                                  color: Color(0xFF38414a),
                                  onPressed: () {
                                    // loginToast('success');
                                    check();
                                  })
                              : 
                                RaisedButton(
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(15.0)),
                                  child: Text(
                                    "Edit",
                                    style: TextStyle(fontSize: 18.0),
                                  ),
                                  textColor: Colors.white,
                                  color: Color(0xFF38414a),
                                  onPressed: () {
                                    enable();
                                  }),
                        ),
                      ],
                    ),
                  ],
                  )
                  )
                  )
                  )
                  ]
                  );
              }
            } else if (snapshot.hasError) {
              return Expanded(
                flex: 5,
                child: Container(
                    color: Colors.red,
                    child: Center(
                        child: Text("${snapshot.error}",
                            style: TextStyle(fontSize: 50.0),
                            textAlign: TextAlign.center))),
              );
            }

            // By default, show a loading spinner.
            return CircularProgressIndicator(
                backgroundColor: Color(0xFF214d8e),
                valueColor:
                    new AlwaysStoppedAnimation<Color>(Colors.cyanAccent));
          },
        ),
      ),
    );
  }
}
