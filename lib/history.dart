import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';

class History {
  String firstname;
  String lastname;
  String destination;
  String citizenOut;
  String citizenIn;

  History(
      {this.firstname,
      this.lastname,
      this.destination,
      this.citizenOut,
      this.citizenIn});

  factory History.fromJson(Map<String, dynamic> json) {
    return History(
    firstname: json['firstname'],
    lastname: json['lastname'],
    destination: json['destination'],
    citizenOut: json['citizen_out'],
    citizenIn: json['citizen_in'],
    );
  }

//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['firstname'] = this.firstname;
//     data['lastname'] = this.lastname;
//     data['destination'] = this.destination;
//     data['citizen_out'] = this.citizenOut;
//     data['citizen_in'] = this.citizenIn;
//     return data;
//   }
}

class HistoryPage extends StatefulWidget {
  final String text;

  HistoryPage({Key key, @required this.text}) : super(key: key);

  @override
  _HistoryPageState createState() => _HistoryPageState();
}

class _HistoryPageState extends State<HistoryPage> {
  Future<List<History>> futureHistory;
  bool isEnabled = false;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    futureHistory = fetchHistory();
  }

  loginToast(String toast) {
    setState(() {
      isLoading = false;
    });
    return Fluttertoast.showToast(
        msg: toast,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.blueAccent,
        textColor: Colors.white);
  }

  enable() {
    if (isEnabled == false) {
      setState(() {
        isEnabled = true;
      });
    } else {
      setState(() {
        isEnabled = false;
      });
    }
  }

  Future<List<History>> fetchHistory() async {
    var text = widget.text;
    final response = await http
        .get('http://192.168.137.1/travelpass/index.php/API/history/$text');
    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      var parsed = json.decode(response.body);

      List jsonResponse = parsed['history'] as List;
      return jsonResponse.map((job) => new History.fromJson(job)).toList();
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load data');
    }
  }

@override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "History",
        ),
        backgroundColor: Color(0xFF214d8e),
        centerTitle: true,
      ),
      body: FutureBuilder<List<History>>(
        future: futureHistory,
        builder: (ctx, snapshot) {
          if (snapshot.hasData) {
            // print(snapshot.data[0].destination);
            List<History> data = snapshot.data;
            print(data[1].destination);
            return SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  // Center(
                  //   child: Text('History'),
                  // ),
                  Padding(
                    padding: EdgeInsets.only(top:10.0
                    ),
                    child: Center(
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: DataTable(
                          sortColumnIndex: 0,
                          sortAscending: true,
                          columns: [
                            DataColumn(
                              label: Text(
                                'First Name',
                                style: TextStyle(
                                  fontSize: 18.0,
                                ),
                              ),
                              numeric: false,
                              tooltip: "First Name",
                            ),
                            DataColumn(
                              label: Text(
                                'Last Name',
                                style: TextStyle(
                                  fontSize: 16.0,
                                ),
                              ),
                              numeric: false,
                              tooltip: 'Last Name',
                            ),
                            DataColumn(
                              label: Text(
                                'Destination',
                                style: TextStyle(
                                  
                                  fontSize: 16.0,
                                ),
                              ),
                              numeric: false,
                              tooltip: "Destination",
                            ),
                            DataColumn(
                              label: Text(
                                'Out',
                                style: TextStyle(
                                 
                                  fontSize: 16.0,
                                ),
                              ),
                              numeric: false,
                              tooltip: "Out",
                            ),
                            DataColumn(
                              label: Text(
                                'In',
                                style: TextStyle(
                                  // color: dDeathColor,
                                  fontSize: 16.0,
                                ),
                              ),
                              numeric: false,
                              tooltip: "In",
                            ),
                          ],
                          rows: data
                              .map(
                                (history) => DataRow(
                                  cells: [
                                    DataCell(
                                      Container(
                                        width: 100,
                                        child: Text(
                                          history.firstname,
                                          softWrap: true,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(fontWeight: FontWeight.w600),
                                        ),
                                      ),
                                    ),
                                    DataCell(
                                      Container(
                                        width: 60.0,
                                        child: Center(
                                          child: Text(
                                            history.lastname,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ),
                                    ),
                                    DataCell(
                                      Center(
                                        child: Text(
                                          history.destination,
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ),
                                    DataCell(
                                      Center(
                                        child: Text(
                                          history.citizenOut??'Not Available',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ),
                                    DataCell(
                                      Center(
                                        child: Text(
                                          history.citizenIn??'Not Available',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                              .toList(),
                        ),
                      ),
                    ),
                  ),
                  // SizedBox(height: 500),
                ],
              ),
            );
          } else if (snapshot.hasError) {
            return AlertDialog(
              title: Text(
                'An Error Occured!',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.redAccent,
                ),
              ),
              content: Text(
                "${snapshot.error}",
                style: TextStyle(
                  color: Colors.blueAccent,
                ),
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text(
                    'Go Back',
                    style: TextStyle(
                      color: Colors.redAccent,
                    ),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          }
          // By default, show a loading spinner.
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CircularProgressIndicator(),
                SizedBox(height: 20),
                Text('This may take some time..')
              ],
            ),
          );
        },
      ),
    );
  }
}
