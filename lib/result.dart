import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Citizen {
  String applicationId;
  String firstname;
  String lastname;
  String birth;
  String age;
  String message;
  String address;
  String gender;
  String email;
  String phone;
  String reason;
  String dateTravel;
  String destination;
  String qrCode;
  String qrLink;
  String schedule;
  String citizenOut;

  Citizen(
      {this.applicationId,
      this.firstname,
      this.lastname,
      this.birth,
      this.age,
      this.message,
      this.address,
      this.gender,
      this.email,
      this.phone,
      this.reason,
      this.dateTravel,
      this.destination,
      this.qrCode,
      this.qrLink,
      this.schedule,
      this.citizenOut});

  Citizen.fromJson(Map<String, dynamic> json) {
    applicationId = json['application_id'];
    firstname = json['firstname'];
    lastname = json['lastname'];
    birth = json['birth'];
    age = json['age'];
    message = json['message'];
    address = json['address'];
    gender = json['gender'];
    email = json['email'];
    phone = json['phone'];
    reason = json['reason'];
    dateTravel = json['date_travel'];
    destination = json['destination'];
    qrCode = json['qr_code'];
    qrLink = json['qr_link'];
    schedule = json['schedule'];
    citizenOut = json['citizen_out'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['application_id'] = this.applicationId;
    data['firstname'] = this.firstname;
    data['lastname'] = this.lastname;
    data['birth'] = this.birth;
    data['age'] = this.age;
    data['message'] = this.message;
    data['address'] = this.address;
    data['gender'] = this.gender;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['reason'] = this.reason;
    data['date_travel'] = this.dateTravel;
    data['destination'] = this.destination;
    data['qr_code'] = this.qrCode;
    data['qr_link'] = this.qrLink;
    data['schedule'] = this.schedule;
    data['citizen_out'] = this.citizenOut;
    return data;
  }
}

class ResultPage extends StatefulWidget {
  final String text;
  final String id;

  ResultPage({Key key, @required this.text, @required this.id})
      : super(key: key);

  @override
  _ResultPageState createState() => _ResultPageState();
}

class _ResultPageState extends State<ResultPage> {
  Future<Citizen> futureCitizen;

  @override
  void initState() {
    super.initState();
    futureCitizen = fetchCitizen();
  }

  Future<Citizen> fetchCitizen() async {
    var text = widget.text;
    var id = widget.id;
    final response = await http
        .get('http://192.168.137.1/travelpass/index.php/API/qr/$text/$id');

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return Citizen.fromJson(json.decode(response.body));
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green,
      appBar: AppBar(
        title: Text(
          "Result",
        ),
        backgroundColor: Color(0xFF214d8e),
        centerTitle: true,
      ),
      body: Center(
          child: FutureBuilder<Citizen>(
              future: futureCitizen,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  if (snapshot.data.firstname != null) {
                    return Container(
                        color: Colors.green,
                        child: Center(
                            child: Text("VALID",
                                style: TextStyle(
                                  fontSize: 50.0,
                                  color: Colors.white,
                                ),
                                textAlign: TextAlign.center)));
                  } else {
                    return Container(
                        color: Colors.red,
                        child: Center(
                            child: Text(snapshot.data.message,
                                style: TextStyle(
                                  fontSize: 50.0,
                                  color: Colors.white,
                                ),
                                textAlign: TextAlign.center)));
                  }
                } else if (snapshot.hasError) {
                  return Container(
                      color: Colors.red,
                      child: Center(
                          child: Text("${snapshot.error}",
                              style: TextStyle(
                                fontSize: 50.0,
                                color: Colors.white,
                              ),
                              textAlign: TextAlign.center)));
                }
                return CircularProgressIndicator(
                    backgroundColor: Color(0xFF214d8e),
                    valueColor:
                        new AlwaysStoppedAnimation<Color>(Colors.cyanAccent));
              })),
      floatingActionButton: new FloatingActionButton(
        onPressed: () {
          showModalBottomSheet(
              context: context,
              builder: (BuildContext bc) {
                return Container(
                  child: FutureBuilder<Citizen>(
                    future: futureCitizen,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        if (snapshot.data.firstname != null) {
                          if (snapshot.data.citizenOut != null) {
                            return ListView(
                              padding: const EdgeInsets.all(8),
                              children: <Widget>[
                                // new Image.asset(
                                //   'assets/cdeo logo.png',
                                //   scale: 4,
                                //   height: 150,
                                // ),
                                Container(
                                  height: 40,
                                  child: Center(
                                      child: Text(
                                          '#${snapshot.data.applicationId}',
                                          style: TextStyle(
                                              fontSize: 30.0,
                                              fontWeight: FontWeight.w700))),
                                ),
                                Container(
                                  height: 40,
                                  child: Center(
                                      child: Text(
                                          '${snapshot.data.firstname} ${snapshot.data.lastname}',
                                          style: TextStyle(
                                              fontSize: 25.0,
                                              fontWeight: FontWeight.w700))),
                                ),
                                Container(
                                  height: 40,
                                  child: Center(
                                      child: Text(
                                          "Address: ${snapshot.data.address}",
                                          style: TextStyle(fontSize: 20.0))),
                                ),
                                Container(
                                  height: 40,
                                  child: Center(
                                      child: Text("Age: ${snapshot.data.age}",
                                          style: TextStyle(fontSize: 20.0))),
                                ),
                                Container(
                                  height: 40,
                                  child: Center(
                                      child: Text(
                                          "Phone: ${snapshot.data.phone}",
                                          style: TextStyle(fontSize: 20.0))),
                                ),
                                new Divider(color: Colors.black),
                                Container(
                                  height: 40,
                                  child: Center(
                                      child: Text(
                                          "Origin: ${snapshot.data.destination}",
                                          style: TextStyle(fontSize: 20.0))),
                                ),
                                Container(
                                  height: 40,
                                  child: Center(
                                      child: Text(
                                          "Reason: ${snapshot.data.reason}",
                                          style: TextStyle(fontSize: 20.0))),
                                ),
                              ],
                            );
                          }
                          return ListView(
                            padding: const EdgeInsets.all(8),
                            children: <Widget>[
                              // new Image.asset(
                              //   'assets/cdeo logo.png',
                              //   scale: 4,
                              //   height: 150,
                              // ),
                              Container(
                                height: 40,
                                child: Center(
                                    child: Text(
                                        '#${snapshot.data.applicationId}',
                                        style: TextStyle(
                                            fontSize: 30.0,
                                            fontWeight: FontWeight.w700))),
                              ),
                              Container(
                                height: 40,
                                child: Center(
                                    child: Text(
                                        '${snapshot.data.firstname} ${snapshot.data.lastname}',
                                        style: TextStyle(
                                            fontSize: 30.0,
                                            fontWeight: FontWeight.w700))),
                              ),
                              Container(
                                height: 40,
                                child: Center(
                                    child: Text(
                                        "Address: ${snapshot.data.address}",
                                        style: TextStyle(fontSize: 20.0))),
                              ),
                              Container(
                                height: 40,
                                child: Center(
                                    child: Text("Age: ${snapshot.data.age}",
                                        style: TextStyle(fontSize: 20.0))),
                              ),
                              Container(
                                height: 40,
                                child: Center(
                                    child: Text("Phone: ${snapshot.data.phone}",
                                        style: TextStyle(fontSize: 20.0))),
                              ),
                              new Divider(color: Colors.black),
                              Container(
                                height: 40,
                                child: Center(
                                    child: Text(
                                        "Destination: ${snapshot.data.destination}",
                                        style: TextStyle(fontSize: 20.0))),
                              ),
                              Container(
                                height: 40,
                                child: Center(
                                    child: Text(
                                        "Reason: ${snapshot.data.reason}",
                                        style: TextStyle(fontSize: 20.0))),
                              ),
                            ],
                          );
                        } else {
                          return Expanded(
                            flex: 5,
                            child: Container(
                                color: Colors.red,
                                child: Center(
                                    child: Text(snapshot.data.message,
                                        style: TextStyle(fontSize: 50.0),
                                        textAlign: TextAlign.center))),
                          );
                        }
                      } else if (snapshot.hasError) {
                        return Expanded(
                          flex: 5,
                          child: Container(
                              color: Colors.red,
                              child: Center(
                                  child: Text("${snapshot.error}",
                                      style: TextStyle(fontSize: 50.0),
                                      textAlign: TextAlign.center))),
                        );
                      }

                      // By default, show a loading spinner.
                      return CircularProgressIndicator(
                          backgroundColor: Color(0xFF214d8e),
                          valueColor: new AlwaysStoppedAnimation<Color>(
                              Colors.cyanAccent));
                    },
                  ),
                );
              });
        },
        child: new Icon(Icons.remove_red_eye),
      ),
    );
  }
}
