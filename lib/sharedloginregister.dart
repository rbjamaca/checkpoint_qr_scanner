import 'dart:convert';

import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:qr_code_scanner/qr_scanner_overlay_shape.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_login/result.dart';
import 'package:flutter_login/profile.dart';
import 'package:flutter_login/history.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

enum LoginStatus { notSignIn, signIn }
enum ConfirmAction { Cancel, Accept }

class _LoginState extends State<Login> {
  LoginStatus _loginStatus = LoginStatus.notSignIn;
  String username, password;
  final _key = new GlobalKey<FormState>();
  bool isLoading = false;

  bool _secureText = true;

  showHide() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  check() {
    final form = _key.currentState;
    if (form.validate()) {
      form.save();
      login();
    }
  }

  login() async {
    setState(() {
      isLoading = true;
    });
    final response =
        await http.post("http://192.168.137.1/travelpass/index.php/api", body: {
      "flag": 1.toString(),
      "username": username,
      "password": password,
      "fcm_token": "test_fcm_token"
    });

    final data = jsonDecode(response.body.toString());
    int value = data['value'];
    String message = data['message'];
    String emailAPI = data['username'];
    String nameAPI = data['name'];
    String id = data['id'];

    if (value == 1) {
      setState(() {
        _loginStatus = LoginStatus.signIn;
        savePref(value, emailAPI, nameAPI, id);
      });
      print(message);
      loginToast(message);
    } else {
      print("fail");
      print(message);
      loginToast(message);
    }
  }

  loginToast(String toast) {
    setState(() {
      isLoading = false;
    });
    return Fluttertoast.showToast(
        msg: toast,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.blueAccent,
        textColor: Colors.white);
  }

  savePref(int value, String username, String name, String id) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.setInt("value", value);
      preferences.setString("name", name);
      preferences.setString("username", username);
      preferences.setString("id", id);
    });
  }

  var value;

  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      value = preferences.getInt("value");

      _loginStatus = value == 1 ? LoginStatus.signIn : LoginStatus.notSignIn;
    });
  }

  signOut() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.setInt("value", null);
      preferences.setString("name", null);
      preferences.setString("username", null);
      preferences.setString("id", null);

      _loginStatus = LoginStatus.notSignIn;
    });
  }

  @override
  void initState() {
    super.initState();
    getPref();
  }

  // log in Widgets
  @override
  Widget build(BuildContext context) {
    switch (_loginStatus) {
      case LoginStatus.notSignIn:
        return Scaffold(
          backgroundColor: Color(0xFF38414a),
          body: Center(
            child: isLoading
                ? Center(
                    child: CircularProgressIndicator(
                        backgroundColor: Color(0xFF214d8e),
                        valueColor: new AlwaysStoppedAnimation<Color>(
                            Colors.cyanAccent)),
                  )
                : ListView(
                    shrinkWrap: true,
                    padding: EdgeInsets.all(15.0),
                    children: <Widget>[
                      Center(
                        child: Container(
                          padding: const EdgeInsets.all(8.0),
                          //            color: Colors.grey.withAlpha(20),
                          color: Color(0xFF214d8e),
                          child: Form(
                            key: _key,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new Image.asset(
                                  'assets/tpass-logo.png',
                                  fit: BoxFit.cover,
                                ),
                                SizedBox(
                                  height: 40,
                                ),
                                SizedBox(
                                  height: 50,
                                  child: Text(
                                    "Log in",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 30.0),
                                  ),
                                ),
                                SizedBox(
                                  height: 25,
                                ),

                                //card for Email TextFormField
                                Card(
                                  elevation: 6.0,
                                  child: TextFormField(
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Please Insert Username";
                                      }
                                      return null;
                                    },
                                    onSaved: (e) => username = e,
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w300,
                                    ),
                                    decoration: InputDecoration(
                                        prefixIcon: Padding(
                                          padding: EdgeInsets.only(
                                              left: 20, right: 15),
                                          child: Icon(Icons.person,
                                              color: Colors.black),
                                        ),
                                        contentPadding: EdgeInsets.all(18),
                                        labelText: "Username"),
                                  ),
                                ),

                                // Card for password TextFormField
                                Card(
                                  elevation: 6.0,
                                  child: TextFormField(
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Password Can't be Empty";
                                      }
                                      return null;
                                    },
                                    obscureText: _secureText,
                                    onSaved: (e) => password = e,
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w300,
                                    ),
                                    decoration: InputDecoration(
                                      labelText: "Password",
                                      prefixIcon: Padding(
                                        padding: EdgeInsets.only(
                                            left: 20, right: 15),
                                        child: Icon(Icons.phonelink_lock,
                                            color: Colors.black),
                                      ),
                                      suffixIcon: IconButton(
                                        onPressed: showHide,
                                        icon: Icon(_secureText
                                            ? Icons.visibility_off
                                            : Icons.visibility),
                                      ),
                                      contentPadding: EdgeInsets.all(18),
                                    ),
                                  ),
                                ),

                                SizedBox(
                                  height: 12,
                                ),

                                // FlatButton(
                                //   onPressed: null,
                                //   child: Text(
                                //     "Forgot Password?",
                                //     style: TextStyle(
                                //         color: Colors.white,
                                //         fontWeight: FontWeight.bold),
                                //   ),
                                // ),

                                Padding(
                                  padding: EdgeInsets.all(14.0),
                                ),

                                new Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    SizedBox(
                                      height: 44.0,
                                      child: RaisedButton(
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(15.0)),
                                          child: Text(
                                            "Login",
                                            style: TextStyle(fontSize: 18.0),
                                          ),
                                          textColor: Colors.white,
                                          color: Color(0xFF38414a),
                                          onPressed: () {
                                            check();
                                          }),
                                    ),
                                    // SizedBox(
                                    //   height: 44.0,
                                    //   child: RaisedButton(
                                    //       shape: RoundedRectangleBorder(
                                    //           borderRadius:
                                    //               BorderRadius.circular(15.0)),
                                    //       child: Text(
                                    //         "GoTo Register",
                                    //         style: TextStyle(fontSize: 18.0),
                                    //       ),
                                    //       textColor: Colors.white,
                                    //       color: Color(0xFF38414a),
                                    //       onPressed: () {
                                    //         Navigator.push(
                                    //           context,
                                    //           MaterialPageRoute(
                                    //               builder: (context) => Register()),
                                    //         );
                                    //       }),
                                    // ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
          ),
        );
        break;

      case LoginStatus.signIn:
        return MainMenu(signOut);
//        return ProfilePage(signOut);
        break;
    }
    return null;
  }
}

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  String name, email, mobile, password;
  final _key = new GlobalKey<FormState>();

  bool _secureText = true;

  showHide() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  check() {
    final form = _key.currentState;
    if (form.validate()) {
      form.save();
      save();
    }
  }

  save() async {
    final response =
        await http.post("http://192.168.8.101/travelpass/index.php/api", body: {
      "flag": 2.toString(),
      "name": name,
      "email": email,
      "mobile": mobile,
      "password": password,
      "fcm_token": "test_fcm_token"
    });

    final data = jsonDecode(response.body);
    int value = data['value'];
    String message = 'Hello, ' + data['name'];
    if (value == 1) {
      setState(() {
        Navigator.pop(context);
      });
      print(message);
      registerToast(message);
    } else if (value == 2) {
      print(message);
      registerToast(message);
    } else {
      print(message);
      registerToast(message);
    }
  }

  registerToast(String toast) {
    return Fluttertoast.showToast(
        msg: toast,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.blueAccent,
        textColor: Colors.white);
  }

  // Register Widget (removed access from ui)
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.all(15.0),
          children: <Widget>[
            Center(
              child: Container(
                padding: const EdgeInsets.all(8.0),
                color: Colors.black,
                child: Form(
                  key: _key,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.network(
                          "https://www.logogenie.net/download/preview/medium/3589659"),
                      SizedBox(
                        height: 40,
                      ),
                      SizedBox(
                        height: 50,
                        child: Text(
                          "Register",
                          style: TextStyle(color: Colors.white, fontSize: 30.0),
                        ),
                      ),
                      SizedBox(
                        height: 25,
                      ),

                      //card for Fullname TextFormField
                      Card(
                        elevation: 6.0,
                        child: TextFormField(
                          validator: (e) {
                            if (e.isEmpty) {
                              return "Please insert Full Name";
                            }
                            return null;
                          },
                          onSaved: (e) => name = e,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w300,
                          ),
                          decoration: InputDecoration(
                              prefixIcon: Padding(
                                padding: EdgeInsets.only(left: 20, right: 15),
                                child: Icon(Icons.person, color: Colors.black),
                              ),
                              contentPadding: EdgeInsets.all(18),
                              labelText: "Fullname"),
                        ),
                      ),

                      //card for Email TextFormField
                      Card(
                        elevation: 6.0,
                        child: TextFormField(
                          validator: (e) {
                            if (e.isEmpty) {
                              return "Please insert Email";
                            }
                            return null;
                          },
                          onSaved: (e) => email = e,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w300,
                          ),
                          decoration: InputDecoration(
                              prefixIcon: Padding(
                                padding: EdgeInsets.only(left: 20, right: 15),
                                child: Icon(Icons.email, color: Colors.black),
                              ),
                              contentPadding: EdgeInsets.all(18),
                              labelText: "Email"),
                        ),
                      ),

                      //card for Mobile TextFormField
                      Card(
                        elevation: 6.0,
                        child: TextFormField(
                          validator: (e) {
                            if (e.isEmpty) {
                              return "Please insert Mobile Number";
                            }
                            return null;
                          },
                          onSaved: (e) => mobile = e,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w300,
                          ),
                          decoration: InputDecoration(
                            prefixIcon: Padding(
                              padding: EdgeInsets.only(left: 20, right: 15),
                              child: Icon(Icons.phone, color: Colors.black),
                            ),
                            contentPadding: EdgeInsets.all(18),
                            labelText: "Mobile",
                          ),
                          keyboardType: TextInputType.number,
                        ),
                      ),

                      //card for Password TextFormField
                      Card(
                        elevation: 6.0,
                        child: TextFormField(
                          obscureText: _secureText,
                          onSaved: (e) => password = e,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w300,
                          ),
                          decoration: InputDecoration(
                              suffixIcon: IconButton(
                                onPressed: showHide,
                                icon: Icon(_secureText
                                    ? Icons.visibility_off
                                    : Icons.visibility),
                              ),
                              prefixIcon: Padding(
                                padding: EdgeInsets.only(left: 20, right: 15),
                                child: Icon(Icons.phonelink_lock,
                                    color: Colors.black),
                              ),
                              contentPadding: EdgeInsets.all(18),
                              labelText: "Password"),
                        ),
                      ),

                      Padding(
                        padding: EdgeInsets.all(12.0),
                      ),

                      new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          SizedBox(
                            height: 44.0,
                            child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0)),
                                child: Text(
                                  "Register",
                                  style: TextStyle(fontSize: 18.0),
                                ),
                                textColor: Colors.white,
                                color: Color(0xFF38414a),
                                onPressed: () {
                                  check();
                                }),
                          ),
                          SizedBox(
                            height: 44.0,
                            child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0)),
                                child: Text(
                                  "GoTo Login",
                                  style: TextStyle(fontSize: 18.0),
                                ),
                                textColor: Colors.white,
                                color: Color(0xFF38414a),
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Login()),
                                  );
                                }),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

//HISTORY

class History {
  String firstname;
  String lastname;
  String destination;
  String citizenOut;
  Null citizenIn;

  History(
      {this.firstname,
      this.lastname,
      this.destination,
      this.citizenOut,
      this.citizenIn});

  History.fromJson(Map<String, dynamic> json) {
    firstname = json['firstname'];
    lastname = json['lastname'];
    destination = json['destination'];
    citizenOut = json['citizen_out'];
    citizenIn = json['citizen_in'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['firstname'] = this.firstname;
    data['lastname'] = this.lastname;
    data['destination'] = this.destination;
    data['citizen_out'] = this.citizenOut;
    data['citizen_in'] = this.citizenIn;
    return data;
  }
}

class MainMenu extends StatefulWidget {
  final VoidCallback signOut;

  MainMenu(this.signOut);

  @override
  _MainMenuState createState() => _MainMenuState();
}

class _MainMenuState extends State<MainMenu> {
  GlobalKey qrKey = GlobalKey();
  var qrText = "";
  var count = "0";
  QRViewController controller;

  // checkCount() async {
  //   final response =
  //       await http.get("http://192.168.137.1/travelpass/index.php/api/count");

  //   final data = jsonDecode(response.body.toString());
  //   setState(() {
  //     count = data['count'];
  //   });
  // }

  signOut() {
    setState(() {
      widget.signOut();
    });
  }

  int currentIndex = 0;
  String selectedIndex = 'TAB: 0';

  String username = "", name = "", id = "";
  TabController tabController;

  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      id = preferences.getString("id");
      username = preferences.getString("username");
      name = preferences.getString("name");
    });
    print("id" + id);
    print("user" + username);
    print("name" + name);
  }

  @override
  void initState() {
    super.initState();
    getPref();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   actions: <Widget>[
      //     IconButton(
      //       onPressed: () {
      //         signOut();
      //       },
      //       icon: Icon(Icons.lock_open),
      //     )
      //   ],
      // ),
      // body: Center(
      //   child: Text(
      //     "WelCome",
      //     style: TextStyle(fontSize: 30.0, color: Colors.blue),
      //   ),
      // ),
      body: Column(
        children: <Widget>[
          Expanded(
            flex: 5,
            child: QRView(
                key: qrKey,
                overlay: QrScannerOverlayShape(
                    borderRadius: 10,
                    borderColor: Colors.green,
                    borderLength: 145,
                    borderWidth: 10,
                    cutOutSize: 250),
                onQRViewCreated: _onQRViewCreate),
          ),
          // Expanded(
          //     flex: 1,
          //     child: Center(
          //       child: Text('Scan Result: $qrText'),
          //     ))
        ],
      ),
      bottomNavigationBar: BottomNavyBar(
        backgroundColor: Colors.black,
        iconSize: 30.0,
//        iconSize: MediaQuery.of(context).size.height * .60,
        currentIndex: currentIndex,
        onItemSelected: (index) {
          setState(() {
            currentIndex = index;
          });
          selectedIndex = 'TAB: $currentIndex';
//            print(selectedIndex);
          reds(selectedIndex);
        },

        items: [
          BottomNavyBarItem(
              icon: Icon(Icons.person),
              title: Text('Profile'),
              activeColor: Color(0xFF38414a)),
          BottomNavyBarItem(
              icon: Icon(Icons.view_list),
              title: Text('List'),
              activeColor: Color(0xFF38414a)),
          BottomNavyBarItem(
            icon: Icon(Icons.exit_to_app),
            title: Text('Log out'),
            activeColor: Color(0xFF38414a),
          ),
        ],
      ),
    );
  }

  //  Action on Bottom Bar Press
  void reds(selectedIndex) async {
//    print(selectedIndex);

    switch (selectedIndex) {
      case "TAB: 0":
        {
          print(id);
          controller.pauseCamera();
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ProfilePage(text: id))).then((id) {
            controller.resumeCamera();
          });
        }
        break;

      case "TAB: 1":
        {
          controller.pauseCamera();
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => HistoryPage(text: id))).then((id) {
            controller.resumeCamera();
          });
        }
        break;

      case "TAB: 2":
        {
          // callToast("Signing out");
          final ConfirmAction action = await _asyncConfirmDialog(context);
          print(action);
        }
        break;
    }
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  void _onQRViewCreate(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      controller.pauseCamera();
      setState(() {
        qrText = scanData;
      });
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ResultPage(text: qrText, id: id)),
      ).then((value) {
        controller.resumeCamera();
      }).then((value) {
        qrText = '';
      });
    });
  }

  callToast(String msg) {
    Fluttertoast.showToast(
        msg: "$msg",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.blueAccent,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  Future<ConfirmAction> _asyncConfirmDialog(BuildContext context) async {
    return showDialog<ConfirmAction>(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Sign out?'),
          content: const Text('You will be signed out from the app.'),
          actions: <Widget>[
            FlatButton(
              child: const Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop(ConfirmAction.Cancel);
              },
            ),
            FlatButton(
              child: const Text('Confirm'),
              onPressed: () {
                Navigator.of(context).pop(ConfirmAction.Accept);
                signOut();
              },
            )
          ],
        );
      },
    );
  }
}
